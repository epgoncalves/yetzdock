
OBS: Este tutorial só funciona no Linux

Passos para instalar o Yetzdock

1- clone o projeto em sua pasta de preferência
    git clone git@gitlab.com:epgoncalves/yetzdock.git

2- Entre na pasta do projeto, em seguida, entre na pasta 'core'
    cd yetzdock/core

3- dentro da pasta 'core', copie o conteúdo do .env-yetz para .env
    cp .env-yetz .env

4- Neste momento, já é possível executar o docker-compose
    docker-compose up -d

########## Se deu tudo certo, agora vamos configurar o projeto.

1- volte para a raiz do yetzdock e entre na pasta 'projetos'
    cd ../projetos

2- Clone aqui o(s) projeto(s) que precisar trabalhar
OBS: lembre sempre de verificar se as credenciais estão alinhadas

    git clone git@gitlab.com:caravela/yetzcards.git

3- Você pode clonar outros projetos em laravel aqui também. No próximo passo, vamos dar uma url para nosso site.


########## Acessando os projetos pelo navegador

1- Primeiro vamos configurar nosso arquivo hosts para criar a url para nosso projeto

OBS: Na pasta core/nginx/sites contém os arquivos de configuração pre-definidos para usarmos no yetzcars e na stag.

2- Acesse o arquivo hosts do seu pc com permissões de root
    sudo nano  /etc/hosts

    no final do arquivo, adicione as entradas e salve o arquivo.

    127.0.0.1       yetz.yetzdock.co
    127.0.0.1       stag.yetzdock.co


    essas url's ficarão disponíveis para acessar somente no navegador do usuário


docker ps --format "{{.Names}}\t{{.Status}}"

yetzdock_nginx_1	Up 9 minutes
yetzdock_php-fpm_1	Up 10 minutes
yetzdock_php-worker_1	Up 10 minutes
yetzdock_phpmyadmin_1	Up 10 minutes
yetzdock_workspace_1	Up 10 minutes
yetzdock_mysql_1	Up 11 minutes
yetzdock_redis_1	Up 11 minutes
yetzdock_minio_1	Up 11 minutes
yetzdock_mongo_1	Up 11 minutes
